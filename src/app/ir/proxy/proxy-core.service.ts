import { Config } from "app/ir/config/config";

import { ConfigService } from './../config/config.service';
import { Response, Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


//核心Http服務
@Injectable()
export class ProxyCoreService {

    protected options;
    protected config: Config;
    private headers:Headers;

    constructor(protected configService: ConfigService, protected http: Http) {
        this.config = configService.getConfig();

        //預設header
        this.headers = new Headers({ 'Content-Type': this.config.jsonContentType });
    }

    public get(webApiUrl: string): Observable<any> {
        return this.http.get(webApiUrl,this.getOption())
            .map(this.extractData)
            .catch(this.handleError);
    }

    public post(webApiUrl: string, dataObj: any): Observable<any> {
        return this.http.post(webApiUrl, dataObj, this.getOption())
            .map(this.extractData)
            .catch(this.handleError);
    }

    private getOption():RequestOptions{
         return new RequestOptions({ headers: this.headers });
    }

    public getHeaders():Headers{
        return this.headers;
    }

    public setHeaders(newHeaders : Headers){
        this.headers = newHeaders;
    }

    //將資料json轉成物件
    private extractData(res: Response) {
        let body = res.json();
        if (body == null) {
            return null;
        }
        return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
