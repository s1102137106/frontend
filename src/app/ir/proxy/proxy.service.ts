import { ConfigService } from './../config/config.service';
import { Headers, Http } from '@angular/http';
import { ProxyCoreService } from './proxy-core.service';
import { AccessService } from './../access/access.service';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

//有access的Http服務
@Injectable()
export class ProxyService extends ProxyCoreService {

  constructor(protected accessService: AccessService, configService: ConfigService, http: Http) {
    super(configService, http);
    this.accessService = accessService;
    //設定accessHeaders
    let accessHeader = this.getAccessHeaders();
    if (accessHeader != null) {
      //將core Service的 header改掉
      this.setHeaders(accessHeader);
    }

  }

  private getAccessHeaders(): Headers {
    //取得權限
    var access = this.accessService.loadAccessInfo();
   
    if (access == null) {
      console.log("access denied");
      return null;
    }
    
    let headers = new Headers({
      'Content-Type': this.config.jsonContentType,
      'Authorization': this.config.tokenType + ' ' + access.Token,
    });
    return headers;
  }




}
