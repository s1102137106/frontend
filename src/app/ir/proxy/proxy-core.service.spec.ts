import { TestBed, inject } from '@angular/core/testing';

import { ProxyCoreService } from './proxy-core.service';
import { ConfigService } from "app/ir/config/config.service";
import { UtilityService } from "app/ir/utility/utility.service";
import { Response, Http, RequestOptions, Headers, ConnectionBackend, HttpModule } from '@angular/http';
describe('ProxyCoreService', () => {
  //要測試的Service
  let proxyCoreService: ProxyCoreService;

  //接收result的model 將any取代成自訂的model
  let result: any

  //設定provider
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        ProxyCoreService,
        ConfigService,
        UtilityService
      ]
    });
  });

  //前置作業 將Service inject進去 利用provider機制
  beforeEach(inject([ProxyCoreService, UtilityService], (proxyCoreService: ProxyCoreService, utilityService: UtilityService) => {
    this.proxyCoreService = proxyCoreService;
    this.utilityService = utilityService;
  }));

  //測試案例 post token username=kent&password=pwd&grant_type=password
  it('取的token body使用字串 username=kent&password=pwd&grant_type=password 預期會成功', () => {
    let headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
    this.proxyCoreService.setHeaders(headers);

    let url = this.utilityService.apiUrlFormat("{0}", "token");
    this.proxyCoreService.post(url,"username=kent&password=pwd&grant_type=password").subscribe(
      (result) => this.result = result,
      error => this.errorMessage = <any>error,
      () => console.log(this.result)
    );

  });

  it('取的token body 使用 物件 預期會失敗', () => {
    let headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
    this.proxyCoreService.setHeaders(headers);

    var data = {
      username: "kent",
      password: "pwd",
      grant_type: "password"
    }

    let url = this.utilityService.apiUrlFormat("{0}", "token");
    this.proxyCoreService.post(url,data).subscribe(
      (result) => this.result = result,
      error => this.errorMessage = <any>error,
      () => console.log(this.result)
    );

  });

  
  it('取的token body 使用 json格式 預期會失敗', () => {
    let headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
    this.proxyCoreService.setHeaders(headers);

    var data = {
      username: "kent",
      password: "pwd",
      grant_type: "password"
    }

    let url = this.utilityService.apiUrlFormat("{0}", "token");
    this.proxyCoreService.post(url,JSON.stringify(data)).subscribe(
      (result) => this.result = result,
      error => this.errorMessage = <any>error,
      () => console.log(this.result)
    );

  });




});
