import { LocalstorageService } from './../localstorage/localstorage.service';
import { AccessService } from './../access/access.service';
import { ProxyService } from './proxy.service';

import { TestBed, inject } from '@angular/core/testing';

import { ConfigService } from "app/ir/config/config.service";
import { UtilityService } from "app/ir/utility/utility.service";
import { Response, Http, RequestOptions, Headers, ConnectionBackend, HttpModule } from '@angular/http';

describe('ProxyService', () => {
  //要測試的Service
  let proxyService: ProxyService;

  //接收result的model 將any取代成自訂的model
  let result: any

  //設定provider
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        ProxyService,
        ConfigService,
        UtilityService,
        AccessService,
        LocalstorageService
      ]
    });
  });

  //前置作業 將Service inject進去 利用provider機制
  beforeEach(inject([ProxyService, UtilityService], (proxyService: ProxyService, utilityService: UtilityService) => {
    this.proxyService = proxyService;
    this.utilityService = utilityService;
  }));
  //測試案例 post token username=kent&password=pwd&grant_type=password
  it('TestApi/Filter body使用物件', () => {
    var data = {
      "CodeType": "object"
    }

    let url = this.utilityService.apiUrlFormat("{0}", "TestApi/Filter");
    this.proxyService.post(url, data).subscribe(
      (result) => this.result = result,
      error => this.errorMessage = <any>error,
      () => console.log(this.result)
    );

  });

  it('TestApi/Filter body使用json', () => {
    var data = {
      "CodeType": "json"
    }

    let url = this.utilityService.apiUrlFormat("{0}", "TestApi/Filter");
    this.proxyService.post(url, JSON.stringify(data)).subscribe(
      (result) => this.result = result,
      error => this.errorMessage = <any>error,
      () => console.log(this.result)
    );

  });
});

