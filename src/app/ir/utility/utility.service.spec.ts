import { ConfigService } from './../config/config.service';

import { TestBed, inject } from '@angular/core/testing';

import { UtilityService } from './utility.service';

describe('UtilityService', () => {
  //需要測試的Service
  let utilityService: UtilityService;

  //前置作業 提供providers
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UtilityService, ConfigService]
    });
  });

  //前置作業 將Service inject進去 利用provider機制
  beforeEach(inject([UtilityService], (utilityService: UtilityService) => {
    this.utilityService = utilityService;
  }));

  it('test isNullOrUndefined method', () => {
    let obj = {}
    //預期的結果要為false
    expect(this.utilityService.isNullOrUndefined(obj)).toEqual(false);

    //預期的結果要為true
    expect(this.utilityService.isNullOrUndefined(undefined)).toEqual(true);

    //預期的結果要為true
    expect(this.utilityService.isNullOrUndefined(null)).toEqual(true);

  });

  it('test apiUrlFormat method', () => {
    let url = this.utilityService.apiUrlFormat("{0}", "api/UserInfo");

    //預期的結果
   //  expect(url).toEqual("http://localhost:59674/api/UserInfo");
  });

  it('test strFormat method', () => {
    let str = this.utilityService.strFormat("Hello {0},{1},{2}", "Jonas", "Kenny", "Kent");

    //預期的結果
    expect(str).toEqual("Hello Jonas,Kenny,Kent");
  });

})
