import { Injectable } from '@angular/core';
import { ConfigService } from "../config/config.service";


@Injectable()
export class UtilityService {
  private config
  constructor(configService:ConfigService) {
    this.config = configService.getConfig();
  }
  /**
   * 判斷物件是否為null 或 undefined
   * 
   * @param {any} obj
   * @returns
   */
  public isNullOrUndefined(obj) {
    if (obj == null || obj == undefined) {
      return true;
    } else {
      return false;
    }
  }

  /**
* 組合api url
* strFormat("url/{1}","B","C")
* @returns
*/
  public apiUrlFormat(...parameter: String[]) {
    var s = this.config.apiRoot + arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
      var reg = new RegExp("\\{" + i + "\\}", "gm");
      s = s.replace(reg, arguments[i + 1]);
    }

    return s;
  }

  /**
  * C# like string.format
  * @example strFormat("A{0}C{1}","B","C")
  * @returns {string} 
  */
  public strFormat(...parameter: String[]) {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
      var reg = new RegExp("\\{" + i + "\\}", "gm");
      s = s.replace(reg, arguments[i + 1]);
    }

    return s;
  }



}
