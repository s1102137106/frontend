//access model
export class Access {
    PerSerilNo: String;
    UserName: String;
    UsrId: String;
    UserMail: String;
    CompanySerilNo: String;
    CompanyName: String;
    CompanyId: String;
    OuId: String;
    OuSerilNo: String;
    OuName: String;
    UserRols: String;
    UserRolKind: String;
    Token: String;
}