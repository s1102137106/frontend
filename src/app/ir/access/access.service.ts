import { Access } from './access';
import { LocalstorageService } from './../localstorage/localstorage.service';
import { Injectable } from '@angular/core';

//權限管理
@Injectable()
export class AccessService {

  constructor(private localstorageService: LocalstorageService) {
    this.localstorageService = localstorageService
  }

  /**
   * 取得accessInfo
   *
   * @return     {Object}  access Info.
   */
  public loadAccessInfo(): Access {
    var testAccess = {
      PerSerilNo: "E20060500008",
      UserName: "許肯尼",
      Token: "_Tz2WmuRzR6EYId0A6suIkg7dg0hqOdhKrb2iXbknFiJiNllifHq0SGC9UOl60mUrH-lt-aYDcQHpWtVa7SEa9c4UGroZx2vyHLrs1g-PN0t9APvbbo26hEGYjqeoKWNewNorU_JJVb3Uq_L8Gxy53waourhXbytyduFqAA1jhWrxqaih1MzNXn-41VaB92ujKKAjIgk8egiOcFnzetD9z8o_Z22QafvjN86mBvYW_UrPHj2cWC5Oyc5QaFAV_ur0fZtnJj_vp6kZyzkijlPqMJfDRDxYIV8_iwszytnE-31D0GpTVYxYAegfofwREaEjZDb3s0XzzHiRD1RqFMYGoMQiW6VIEa3EC0lOviLSn7sr6YGSoLByrEuZbryai4pzxv2sx1KvCN71XGcD4QJqYQvG3FTGBZSMrsQf2yZv0Ygh0AkT7tyEy1Ea6YzBCSoSv-Xwyu4HyzpZC02Pp6ZgcO9alYa_IPwaclWDvksLOzo_x5RNDe9SvHwmL8wLcdCn0SNhSRT5rUsE4tvLTbj6ikVZ8ptwg3OzFRpTMCCFXhy4iClbxsCFm7a6JgzfIS6"
    }
    this.setAccessInfo(testAccess);
    let data = this.localstorageService.loadJobj("accessInfo");

    if (data == null) {
      return null;
    }

    var access: Access = {
        PerSerilNo : data.PerSerilNo,//"E20060500008";
        UserName : data.UserName,//"許肯尼";
        Token : data.Token,//"awer2j139adfjkli"
        UsrId: "",
        UserMail:  "",
        CompanySerilNo:  "",
        CompanyName:  "",
        CompanyId:  "",
        OuId:  "",
        OuSerilNo:  "",
        OuName:  "",
        UserRols:  "",
        UserRolKind:  ""
    }
    
    return access;

  }

  public setAccessInfo(data) {
    this.localstorageService.setJobj("accessInfo", data);
  }



}

