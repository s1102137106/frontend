//config model
export class Config {
    gridPagesize: number;
    jsonContentType: string;
    formurlencodedContentType: string;
    apiRoot: string;
    tokenType: string;
}