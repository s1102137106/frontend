import { environment } from './../../../environments/environment.prod';
import { TestBed, inject } from '@angular/core/testing';

import { ConfigService } from './config.service';

describe('ConfigService', () => {
  //需要測試的Service
  let configService: ConfigService;

  //前置作業 提供providers
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConfigService]
    });
  });

  //前置作業 將Service inject進去 利用provider機制
  beforeEach(inject([ConfigService], (configService: ConfigService) => {
    this.configService = configService;
  }));

  it('test getConfig method', () => {
    console.log("test getConfig method")
    console.log(this.configService.getConfig())
  });
});
