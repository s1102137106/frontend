import { UtilityService } from './../utility/utility.service';

import { Injectable, Inject, forwardRef } from '@angular/core';
import { Config } from "app/ir/config/config";
import { environment } from '../../../environments/environment';

@Injectable()
export class ConfigService {

  private config: Config;

  constructor() {
    this.config = CONFIG;
  }

  /**
  * 取得config檔案
  * 
  */
  public getConfig() {
    return this.config;
  }

}

//預設設定
export const CONFIG: Config = {
  //Grid 每頁預設筆數
  gridPagesize: 10,

  //Http ContentType
  jsonContentType: "application/json",
  formurlencodedContentType: "application/x-www-form-urlencoded",
  apiRoot: environment.rootApiUrl,
  tokenType: "Bearer",


}
/*
export enum WebApiUrlType {
    EmployeeList,//employee下拉式選單
}
*/