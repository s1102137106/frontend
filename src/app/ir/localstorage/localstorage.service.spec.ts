import { TestBed, inject } from '@angular/core/testing';

import { LocalstorageService } from './localstorage.service';

describe('LocalstorageService', () => {

  //需要測試的Service
  let localstorageService: LocalstorageService;

  //前置作業 提供providers
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalstorageService]
    });
  });

  //前置作業 將Service inject進去 利用provider機制
  beforeEach(inject([LocalstorageService], (localstorageService: LocalstorageService) => {
    this.localstorageService = localstorageService;
  }));

  it('test set method and load method', () => {
    let value = "Jonas"
    let key = "Name"

    this.localstorageService.set(key, value);

    //預期取出來的值 跟設定進去的一樣
    expect(this.localstorageService.load(key)).toEqual(value);

  });

  it('test setJobj method and loadJobj method', () => {
    let value = [{ "name": "Jonas", "id": 9999 }]
    let key = "testUserInfo"

    this.localstorageService.setJobj(key, value);
    //預期取出來的值 跟設定進去的一樣
    expect(this.localstorageService.loadJobj(key)).toEqual(value);
  });
});
