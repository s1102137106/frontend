import { Injectable } from '@angular/core';

@Injectable()
export class LocalstorageService {

  constructor() { }
  /**
   * 設定localStorage
   * 
   * @param {string} key
   * @param {any} val
   */
  public set(key, val) {
    localStorage.setItem(key, val);
  }

  /**
   * 設定 json 到localStorage
   * 
   * @param {string} key
   * @param {any} jobj
   */
  public setJobj(key, jobj) {
    localStorage.setItem(key, JSON.stringify(jobj));
  }

  /**
   * 取localStorage 資料
   * 
   * @param {string} key
   * @returns 設定localStorage data
   */
  public load(key) {
    return localStorage.getItem(key)
  }

  /**
   * 取localStorage中的json 資料
   * 
   * @param {string} key
   * @returns 設定localStorage json data
   */
  public loadJobj(key) {
    return JSON.parse(localStorage.getItem(key))
  }
}
