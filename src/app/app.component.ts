import { ProxyService } from './ir/proxy/proxy.service';
import { ProxyCoreService } from './ir/proxy/proxy-core.service';

import { ConfigService } from './ir/config/config.service';
import { Component } from '@angular/core';
import { Config } from "app/ir/config/config";
import { UtilityService } from "app/ir/utility/utility.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private employees;
  private errorMessage;

  constructor(private proxyCoreService: ProxyCoreService, private proxyService: ProxyService, private utilityService: UtilityService) {
    this.utilityService = utilityService;
    this.proxyCoreService = proxyCoreService;
    this.proxyService = proxyService;

    let url = this.utilityService.apiUrlFormat("{0}","odata/Employees");
    this.proxyService.get(url).subscribe(
      (result) => this.employees = result,
      error => this.errorMessage = <any>error,
      () => this.successCallBack());

    var name = "Jonas";
   console.log( this.utilityService.strFormat("Hello {0}", name));
  }

  private successCallBack() {

  }

  title = 'app works!';
}
