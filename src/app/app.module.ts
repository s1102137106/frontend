import { ProxyService } from './ir/proxy/proxy.service';
import { ProxyCoreService } from './ir/proxy/proxy-core.service';
import { AccessService } from './ir/access/access.service';
import { LocalstorageService } from './ir/localstorage/localstorage.service';
import { UtilityService } from './ir/utility/utility.service';
import { ConfigService } from './ir/config/config.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    ConfigService,
    UtilityService,
    LocalstorageService,
    AccessService,
    ProxyCoreService,
    ProxyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
